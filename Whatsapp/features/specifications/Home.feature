# language: pt

Funcionalidade: Pagina do WhatsApp
Eu como um estutante de tecnologia
Gostaria de me comunicar coms os meus amigos de forma fácil
Para conversar sobre varios assuntos

@smoke
Cenário: Espero que ao acessar o pagina do WhatsApp nao seja exibido nenhum erro
  Dado que eu esteja logado na pagina principal do WhatsApp
  Quando a pagina terminar de carregar
  Então nao deve exibir nenhum tipo de erro

@wip
Cenário: Espero que ao buscar um contato valido o mesmo seja exibido na lista
  Quando eu pesquisar um contato valido
  Então deve ser exibido na lista
