require "capybara/cucumber"
require "selenium-webdriver"
require "site_prism"


Capybara.current_driver = :selenium
Capybara.default_driver = :selenium
Capybara.default_max_wait_time = 5


def init
  local_driver
end


def local_driver
    Capybara.register_driver :selenium do |app|
        profile = get_profile
        Capybara::Selenium::Driver.new(app, :profile => profile)
    end
end

def get_profile
    profile = Selenium::WebDriver::Firefox::Profile.new
    profile["network.proxy.http"] = ""
    profile["network.proxy.http_port"] = ""
    profile["network.proxy.type"] = 3
    profile
end

init
