### Dado ###
Dado(/^que eu esteja logado na pagina principal do WhatsApp$/) do
  @home_page = Home.new
end

### Quando ###
Quando(/^a pagina terminar de carregar$/) do
@home_page.load
end

Quando(/^eu pesquisar um contato valido$/) do
@home_page.search "Miguel"
end

### Entao ###
Então(/^nao deve exibir nenhum tipo de erro$/) do
  expect(@home_page).to have_content 'Miguel Araujo'
  verifyErrosInPage @result_page
end

Então(/^deve ser exibido na lista$/) do

end

### Outros ###
def verifyErrosInPage resultPage
    @badWords = BadWords.new
    @words = @badWords.getWords
    @words.each do |word|
        expect(resultPage).to have_no_content(word)
    end
end
