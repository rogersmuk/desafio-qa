class Home < SitePrism::Page
    set_url "https://web.whatsapp.com/"
    element :search_fild, ".cont-input-search"

    def search termo
        search_fild.set termo
    end
end
