class Venda
  $produtos = []
  $total = 0.0

  def preco listaCompra
    $produtos = listaCompra

    $i = 0

    while $produtos.size > 0 do
      calcula $produtos[$i]
    end

    puts $total
  end


  def removeItem item
   $produtos.delete(item)
  end

  def calcula item
    qtde = $produtos.count(item)
    resultado = qtde.divmod item.multiplo

    if item.multiplo == 1
      $total += (resultado[0] * item.valor_unitario)
    else
      $total += (resultado[0] * item.valor_Combo)
      $total += (resultado[1] * item.valor_unitario)
    end

    removeItem item
  end

end

class CadastroPromocao
  attr_accessor :codigo, :descricao, :valor_unitario, :valor_Combo, :multiplo
end


A = CadastroPromocao.new
A.codigo = "A"
A.descricao = "Promocao leve 3 pague 2"
A.valor_unitario = 50.0
A.valor_Combo = 130.0
A.multiplo = 3

B = CadastroPromocao.new
B.codigo = "B"
B.descricao = "Promocao leve 2 por 45"
B.valor_unitario = 30.0
B.valor_Combo = 45.0
B.multiplo = 2

C = CadastroPromocao.new
C.codigo = "C"
C.descricao = "Não tem promoção"
C.valor_unitario = 20.0
C.valor_Combo = 0.0
C.multiplo = 1

D = CadastroPromocao.new
D.codigo = "D"
D.descricao = "Não tem"
D.valor_unitario = 15.0
D.valor_Combo = 0.0
D.multiplo = 1

lista_itens = [C,D,B,A] #115
#lista_itens = [A,A,A,A,A,A] #260
#lista_itens = [A,A,A,B,B] #175

venda = Venda.new
venda.preco lista_itens
